﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Chronomical.General.Runtime
{
    /// <summary>
    /// Retrieve static info from the device
    /// </summary>
    public static class StaticInfo
    {
        #region Class variables
        /// <summary>
        /// Persistent data path for Chronomical utilities. Creates the directory if it does not yet exits
        /// </summary>
        public static string ChronomicalPersistentDataPath 
        {
            get
            {
                string path = Path.Combine(Application.persistentDataPath, "Chronomical");
                Directory.CreateDirectory(path); //Creating the directory just to be sure
                return path;
            }
        }
        #endregion
    }
}
