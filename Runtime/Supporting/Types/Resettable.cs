﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.General.Runtime.Supporting.Types
{
    /// <summary>
    /// Object with built-in method to reset itself to its original value
    /// </summary>
    /// <typeparam name="T">Object type</typeparam>
    public class Resettable<T>
    {
        #region Class variables
        public T objectField;

        //Private
        T _initObjectField;
        #endregion

        #region Constructor
        public Resettable(T objectField)
        {
            this.objectField = objectField;
            _initObjectField = objectField;
        }
        #endregion

        #region Class specific methods
        public void Reset() => objectField = _initObjectField;
        #endregion
    }
}