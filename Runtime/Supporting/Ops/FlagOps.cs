﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.General.Runtime.Supporting.Ops
{
    /// <summary>
    /// Enum flag operations
    /// </summary>
    public static class FlagOps
    {
        /// <summary>
        /// Checks whether enum contains all of the flags
        /// </summary>
        /// <typeparam name="T">Enum type to check</typeparam>
        /// <param name="toCheck">Enum to check for flags</param>
        /// <param name="flags">The flags it should contain</param>
        /// <returns>Whether the given enum contains all of the passed flags</returns>
        public static bool HasFlags<T>(T toCheck, params T[] flags) where T : Enum
        {
            foreach(T flag in flags) if (!toCheck.HasFlag(flag)) return false; 
            return true;
        }

        /// <summary>
        /// Checks whether a set of enums all contain a particular flag
        /// </summary>
        /// <typeparam name="T">Enum type to check</typeparam>
        /// <param name="flag">The flag to check</param>
        /// <param name="toCheck">The enum set</param>
        /// <returns>Bool based on whether all in the given set contain the given flag</returns>
        public static bool AllContainFlag<T>(T flag, params T[] toCheck) where T : Enum
        {
            foreach (T tCheck in toCheck) if (!tCheck.HasFlag(flag)) return false;
            return true;
        }

        /// <summary>
        /// Checks whether in a set of enums, at least one contains the given flag
        /// </summary>
        /// <typeparam name="T">Enum type to check</typeparam>
        /// <param name="flag">The flag to check</param>
        /// <param name="toCheck">The enum set</param>
        /// <returns>Bool based on whether at least in the given set contains the given flag</returns>
        public static bool AnyContainsFlag<T>(T flag, params T[] toCheck) where T : Enum
        {
            foreach (T tCheck in toCheck) if (tCheck.HasFlag(flag)) return true;
            return false;
        }
    }
}