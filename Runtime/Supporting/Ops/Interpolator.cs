﻿using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace Chronomical.General.Runtime.Supporting.Ops
{
    /// <summary>
    /// Contains linear interpolation methods
    /// </summary>
    public static class Interpolator
    {
        #region Gameobject
        /// <summary>
        /// Linear interpolation move function in world space
        /// </summary>
        /// <param name="gameobject">The transform to manipulate</param>
        /// <param name="dest">The detination in world space</param>
        /// <param name="time">The time in seconds the move should take</param>
        /// <param name="easeInOut">Whether smoothing should be applied</param>
        public static async void InterpolateMove(Transform gameobject, Vector3 dest, float time, bool easeInOut = false)
        {
            Vector3 start = gameobject.position;
            float startTime = Time.time;

            while(gameobject.position != dest)
            {
                if (gameobject == null) return;
                if(easeInOut) gameobject.position = Vector3.Lerp(start, dest, Mathf.SmoothStep(0, 1, (Time.time - startTime) / time));
                else gameobject.position = Vector3.Lerp(start, dest, (Time.time - startTime) / time);
                await Task.Delay(10);
            }
        }

        /// <summary>
        /// Linear interpolation move function in local space
        /// </summary>
        /// <param name="gameobject">The transform to manipulate</param>
        /// <param name="dest">The detination in local space</param>
        /// <param name="time">The time in seconds the move should take</param>
        /// <param name="easeInOut">Whether smoothing should be applied</param>
        public static async void InterpolateMoveLocal(Transform gameobject, Vector3 dest, float time, bool easeInOut = false)
        {
            Vector3 start = gameobject.localPosition;
            float startTime = Time.time;

            while (gameobject.localPosition != dest)
            {
                if (gameobject == null) return;
                if (easeInOut) gameobject.localPosition = Vector3.Lerp(start, dest, Mathf.SmoothStep(0, 1, (Time.time - startTime) / time));
                else gameobject.localPosition = Vector3.Lerp(start, dest, (Time.time - startTime) / time);
                await Task.Delay(10);
            }
        }

        /// <summary>
        /// Linear interpolation rotation function in world space
        /// </summary>
        /// <param name="gameobject">The transform to manipulate</param>
        /// <param name="targetRot">The target rotation in world space</param>
        /// <param name="time">The time in seconds the rotation should take</param>
        /// <param name="easeInOut">Whether smoothing should be applied</param>
        public static async void InterpolateRotate(Transform gameobject, Vector3 targetRot, float time, bool easeInOut = false)
        {
            Quaternion startRot = gameobject.rotation;
            float startTime = Time.time;

            while (gameobject.rotation.eulerAngles != targetRot)
            {
                if (gameobject == null) return;
                if(easeInOut) gameobject.rotation = Quaternion.Lerp(startRot, Quaternion.Euler(targetRot), Mathf.SmoothStep(0, 1, (Time.time - startTime) / time));
                else gameobject.rotation = Quaternion.Lerp(startRot, Quaternion.Euler(targetRot), (Time.time - startTime) / time);
                await Task.Delay(10);
            }
        }

        /// <summary>
        /// Linear interpolation rotation function in local space
        /// </summary>
        /// <param name="gameobject">The transform to manipulate</param>
        /// <param name="targetRot">The target rotation in local space</param>
        /// <param name="time">The time in seconds the rotation should take</param>
        /// <param name="easeInOut">Whether smoothing should be applied</param>
        public static async void InterpolateRotateLocal(Transform gameobject, Vector3 targetRot, float time, bool easeInOut = false)
        {
            Quaternion startRot = gameobject.localRotation;
            float startTime = Time.time;

            while (gameobject.localRotation.eulerAngles != targetRot)
            {
                if (gameobject == null) return;
                if (easeInOut) gameobject.localRotation = Quaternion.Lerp(startRot, Quaternion.Euler(targetRot), Mathf.SmoothStep(0, 1, (Time.time - startTime) / time));
                else gameobject.localRotation = Quaternion.Lerp(startRot, Quaternion.Euler(targetRot), (Time.time - startTime) / time);
                await Task.Delay(10);
            }
        }

        /// <summary>
        /// Linear interpolation move and rotation function in world space
        /// </summary>
        /// <param name="gameobject">The transform to manipulate</param>
        /// <param name="dest">The detination in world space</param>
        /// <param name="targetRot">The target rotation in world space</param>
        /// <param name="time">The time in seconds the transform should take</param>
        /// <param name="easeInOut">Whether smoothing should be applied</param>
        public static async void InterpolateTransform(Transform gameobject, Vector3 dest, Vector3 targetRot, float time, bool easeInOut = false)
        {
            Vector3 start = gameobject.position;
            Quaternion startRot = gameobject.rotation;
            float startTime = Time.time;

            while (gameobject.position != dest)
            {
                if (gameobject == null) return;
                float progression = easeInOut ? Mathf.SmoothStep(0, 1, (Time.time - startTime) / time) : (Time.time - startTime) / time;
                gameobject.position = Vector3.Lerp(start, dest, progression);
                gameobject.rotation = Quaternion.Lerp(startRot, Quaternion.Euler(targetRot), progression);
                await Task.Delay(10);
            }
        }

        /// <summary>
        /// Linear interpolation move and rotation function in local space
        /// </summary>
        /// <param name="gameobject">The transform to manipulate</param>
        /// <param name="dest">The detination in local space</param>
        /// <param name="targetRot">The target rotation in local space</param>
        /// <param name="time">The time in seconds the transform should take</param>
        /// <param name="easeInOut">Whether smoothing should be applied</param>
        public static async void InterpolateTransformLocal(Transform gameobject, Vector3 dest, Vector3 targetRot, float time, bool easeInOut = false)
        {
            Vector3 start = gameobject.localPosition;
            Quaternion startRot = gameobject.localRotation;
            float startTime = Time.time;

            while (gameobject.localPosition != dest)
            {
                if (gameobject == null) return;
                float progression = easeInOut ? Mathf.SmoothStep(0, 1, (Time.time - startTime) / time) : (Time.time - startTime) / time;
                gameobject.localPosition = Vector3.Lerp(start, dest, progression);
                gameobject.localRotation = Quaternion.Lerp(startRot, Quaternion.Euler(targetRot), progression);
                await Task.Delay(10);
            }
        }
        #endregion
    }
}