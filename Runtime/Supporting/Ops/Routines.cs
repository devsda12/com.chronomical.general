﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.General.Runtime.Supporting.Ops
{
    /// <summary>
    /// Simple usage coroutines
    /// </summary>
    public static class SimpleRoutine
    {
        /// <summary>
        /// Invoke an action type callback after a given amount of seconds
        /// </summary>
        /// <param name="secWait">The amount of seconds to wait</param>
        /// <param name="callback">The callback to call after the set time has elapsed</param>
        /// <returns></returns>
        public static IEnumerator ActionOnSec(float secWait, Action callback)
        {
            yield return new WaitForSeconds(secWait);
            callback.Invoke();
        }
    }
}