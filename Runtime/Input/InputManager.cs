﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Text;
using UnityEngine;

namespace Chronomical.General.Runtime.Input
{
    #region Namespace classes
    [Serializable]
    public class Keybind
    {
        //Keybind region
        public string bindName;
        public KeyCode keyCode;

        #if UNITY_EDITOR
        [NonSerialized] public bool isFolded;
        #endif
    }

    [Serializable]
    public class Axis
    {
        //Axis region
        public string axisName;
        public KeyCode positive, negative;
        public float valueGravity = 1;

        //Runtime variables
        [NonSerialized] public KeyCode cachedPress;
        [NonSerialized] public float cachedValue;

        #if UNITY_EDITOR
        [NonSerialized] public bool isFolded;
        #endif
    }

    [Serializable]
    public class SaveWrapper
    {
        public List<Keybind> keybinds;
        public List<Axis> axes;
    }
    #endregion

    public class InputManager
    {
        #region Class variables
        //Const
        public readonly static string fileName = "InputManagerProfile.json";
        string filePath
        {
            get => Path.Combine(StaticInfo.ChronomicalPersistentDataPath, fileName);
        }
        string defaultFilePath
        {
            get => Path.Combine(Application.streamingAssetsPath, fileName);
        }

        //Instance
        static InputManager _instance;
        public static InputManager Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = new InputManager();
                    _instance.Load();
                }
                return _instance;
            }
            private set { }
        }

        //Lists with keybinds and axes
        public List<Keybind> keybinds = new List<Keybind>();
        public List<Axis> axes = new List<Axis>();
        #endregion

        #region Loading / Saving / Resetting
        /// <summary>
        /// Writes the keybinds to the JSON file from memory
        /// </summary>
        void Save()
        {
            SaveWrapper save = new SaveWrapper() { keybinds = this.keybinds, axes = this.axes };
            File.WriteAllText(filePath, JsonUtility.ToJson(save, true));
        }

        /// <summary>
        /// Loads the keybinds from the JSON file into memory
        /// </summary>
        void Load()
        {
            //Resetting if not created yet
            if (!File.Exists(filePath) || Application.isEditor) Reset();

            //Reading the lines and deserializing it from json
            SaveWrapper load = JsonUtility.FromJson<SaveWrapper>(File.ReadAllText(filePath));
            keybinds = load.keybinds;
            axes = load.axes;
        }

        /// <summary>
        /// Resets the keybinds to factory settings ;)
        /// </summary>
        void Reset()
        {
            //Copying the defaults
            File.Delete(filePath);
            File.Copy(defaultFilePath, filePath);
        }
        #endregion

        #region Keybind setting methods
        /// <summary>
        /// Sets the keybind related to bindName. If bind does not exist, nothing happens
        /// </summary>
        /// <param name="bindName">The name of the keybind</param>
        /// <param name="newKey">The new key to associate with the bind</param>
        /// <param name="flushBuffer">Wether to flush the memory into permanent storage (e.g. save to file)</param>
        public void SetKey(string bindName, KeyCode newKey, bool flushBuffer = false)
        {
            Keybind foundBind = GetKeyBindByName(bindName);
            if(foundBind != null) foundBind.keyCode = newKey;
            if (flushBuffer) Save();
        }
        #endregion

        #region Keybind evaluation methods - Supporting
        /// <summary>
        /// Gets a saved keybind by name of the bind
        /// </summary>
        /// <param name="bindName">The name of the bind to check the keybind for</param>
        /// <returns>The keybind belonging to the action name. If nothing found, returns null</returns>
        public Keybind GetKeyBindByName(string bindName) => keybinds.Find(keyB => keyB.bindName == bindName);
        #endregion

        #region Keybind evaluation methods
        /// <summary>
        /// Returns true during all frames the key is pushed down
        /// </summary>
        /// <param name="bindName">The name of the bind to check the key is held down for</param>
        /// <returns>Boolean based on the result of the expression</returns>
        public bool GetKey(string bindName)
        {
            Keybind foundBind = GetKeyBindByName(bindName);
            return foundBind != null ? UnityEngine.Input.GetKey(foundBind.keyCode) : false;
        }

        /// <summary>
        /// Returns true during the first frame the key is pushed down
        /// </summary>
        /// <param name="bindName">The name of the bind to check the key is held down for</param>
        /// <returns>Boolean based on the result of the expression</returns>
        public bool GetKeyDown(string bindName)
        {
            Keybind foundBind = GetKeyBindByName(bindName);
            return foundBind != null ? UnityEngine.Input.GetKeyDown(foundBind.keyCode) : false;
        }

        /// <summary>
        /// Returns true during the frame the key is let go of
        /// </summary>
        /// <param name="bindName">The name of the action to check the key is let go for</param>
        /// <returns>Boolean based on the result of the expression</returns>
        public bool GetKeyUp(string bindName)
        {
            Keybind foundBind = GetKeyBindByName(bindName);
            return foundBind != null ? UnityEngine.Input.GetKeyUp(foundBind.keyCode) : false;
        }
        #endregion

        #region Axis setting methods
        public void SetAxis(string axisName, KeyCode posKey, KeyCode negKey, bool flushBuffer = false)
        {
            Axis foundAxis = GetAxisByName(axisName);
            if(foundAxis != null)
            {
                foundAxis.positive = posKey;
                foundAxis.negative = negKey;
            }
            if (flushBuffer) Save();
        }
        #endregion

        #region Axis evaluation methods - Supporting
        /// <summary>
        /// Gets a saved axis by name of the axis
        /// </summary>
        /// <param name="axisName">The name of the axis</param>
        /// <returns>The axis which contains the axisName. If nothing found, returns null</returns>
        public Axis GetAxisByName(string axisName) => axes.Find(axe => axe.axisName == axisName);
        #endregion

        #region Axis evaluation methods
        public float GetAxis(string axisName)
        {
            //Checking if it is indeed an existing axis
            Axis concerningAxis = GetAxisByName(axisName);
            if (concerningAxis != null)
            {
                //Now we got the axis, returning the value it has using an evaluation of the cachedpress
                if(concerningAxis.cachedPress != concerningAxis.negative && UnityEngine.Input.GetKey(concerningAxis.positive))
                {
                    concerningAxis.cachedPress = concerningAxis.positive;
                    concerningAxis.cachedValue += 0.05f * concerningAxis.valueGravity;
                    return concerningAxis.cachedValue = Mathf.Clamp(concerningAxis.cachedValue, -1, 1);
                } else if(concerningAxis.cachedPress != concerningAxis.positive && UnityEngine.Input.GetKey(concerningAxis.negative))
                {
                    concerningAxis.cachedPress = concerningAxis.negative;
                    concerningAxis.cachedValue -= 0.05f * concerningAxis.valueGravity;
                    return concerningAxis.cachedValue = Mathf.Clamp(concerningAxis.cachedValue, -1, 1);
                } else
                {
                    concerningAxis.cachedPress = KeyCode.None;
                    return concerningAxis.cachedValue == 0 ? concerningAxis.cachedValue : concerningAxis.cachedValue = Mathf.MoveTowards(concerningAxis.cachedValue, 0, 0.05f * concerningAxis.valueGravity);
                }
            }
            return 0;
        }

        public float GetAxisRaw(string axisName)
        {
            Axis concerningAxis = GetAxisByName(axisName);
            if(concerningAxis != null)
            {
                if (UnityEngine.Input.GetKey(concerningAxis.positive)) return 1;
                else if (UnityEngine.Input.GetKey(concerningAxis.negative)) return 0;
            }
            return 0;
        }
        #endregion
    }
}
