﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using System.Linq;
//using UnityEngine;
//using UnityEngine.InputSystem;
//using UnityEngine.InputSystem.Controls;

//namespace Chronomical.Input.InputSystem
//{
//    #region Namespace classes
//    /// <summary>
//    /// Keyboard keybind
//    /// </summary>
//    [Serializable]
//    public class Keybind
//    {
//        //Config fields
//        public string bindName;
//        public Key keyCode;
//        public ButtonControl buttonCode;

//        //Editor variables
//        public bool forKeyboard, forGamepad;
//    }

//    /// <summary>
//    /// Axis keybind
//    /// </summary>
//    [Serializable]
//    public class Axis
//    {
//        //Config fields
//        public string axisName;
//        public Keybind positive, negative;

//        //Runtime variables
//        public Keybind cachedPress;
//        public float cachedValue;
//    }

//    /// <summary>
//    /// Wrapper for saving the keybinds
//    /// </summary>
//    [Serializable]
//    public class SaveWrapper
//    {
//        public List<Keybind> keybinds;
//        public List<Axis> axes;
//    }
//    #endregion

//    public class InputSystemManager
//    {
//        #region Class variables
//        //File IO variables
//        const string fileName = "InputProfile.json";
//        string filePath
//        {
//            get
//            {
//                return Path.Combine(StaticInfo.ChronomicalPersistentDataPath, fileName);
//            }
//        }

//        //Devices
//        Keyboard CurrentKeyboard
//        {
//            get => Keyboard.current;
//        }

//        Gamepad CurrentGamepad
//        {
//            get => Gamepad.current;
//        }

//        //Instance
//        static InputSystemManager _instance;
//        public static InputSystemManager Instance
//        {
//            get
//            {
//                if (_instance == null)
//                {
//                    _instance = new InputSystemManager();
//                    _instance.Load();
//                }
//                return _instance;
//            }
//        }

//        //Keybind / axes lists
//        List<Keybind> keybinds = new List<Keybind>();
//        List<Axis> axes = new List<Axis>();
//        #endregion

//        #region Loading / Saving / Resetting / Clearing memory
//        void Save()
//        {
//            if (keybinds.Count == 0) return;

//            //SaveWrapper save = new SaveWrapper() { keybinds = keybinds, axes = axes };
//            //File.WriteAllText(filePath, JsonUtility.ToJson(save, true));
//        }

//        void Load()
//        {
//            if (!File.Exists(filePath)) Save(); //TODO -> load defaults

//            keybinds.Add(new Keybind() { bindName="TestBind", forKeyboard=true, keyCode=Key.E });

//            //SaveWrapper load = JsonUtility.FromJson<SaveWrapper>(File.ReadAllText(filePath));
//            //keybinds = load.keybinds;
//            //axes = load.axes;
//        }

//        /// <summary>
//        /// Clears the memory of any keybinds
//        /// </summary>
//        public void ClearMemory() { keybinds.Clear(); axes.Clear(); }

//        /// <summary>
//        /// Clears the file of any keybinds
//        /// </summary>
//        public void ClearFile() { File.Delete(filePath); }
//        #endregion

//        #region Keybind getting methods - Supporting
//        Keybind GetKeybindByName(string name)
//        {
//            foreach(Keybind keybind in keybinds)
//            {
//                if (keybind.bindName == name) return keybind;
//            }
//            return null;
//        }
//        #endregion

//        #region Keybind getting methods
//        /// <summary>
//        /// Returns true every frame the user presses the associated keybind
//        /// </summary>
//        /// <param name="bindName">The keybind name to check</param>
//        /// <returns>Boolean based on whether the user has pressed any keys associated with this keybind</returns>
//        public bool GetBind(string bindName)
//        {
//            Keybind foundKeybind;
//            if ((foundKeybind = GetKeybindByName(bindName)) == null) return false;

//            if (foundKeybind.forKeyboard && CurrentKeyboard.allKeys.Where(keyC => keyC.keyCode == foundKeybind.keyCode).First().isPressed) return true;
//            //if (foundKeybind.forGamepad && foundKeybind.buttonCode.isPressed) return true;
//            return false;
//        }

//        //public bool GetBindDown(string bindName)
//        //{

//        //}

//        //public bool GetBindUp(string bindName)
//        //{

//        //}
//        #endregion
//    }
//}