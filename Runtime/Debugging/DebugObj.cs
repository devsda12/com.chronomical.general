﻿using Chronomical.General.Runtime.Input;
using UnityEngine;

namespace Chronomical.General.Runtime.Debugging
{
    public class DebugObj : MonoBehaviour
    {
        #region Unity methods
        private void Update()
        {
            Debug.Log(InputManager.Instance.GetKey("Jump"));
            Debug.Log(InputManager.Instance.GetAxis("Horizontal"));

            //Debug.Log(Input.Input.Instance.GetBind("TestBind"));

            //Keyboard keyboard = Keyboard.current;
            //if (keyboard == null) return;
            //if (keyboard.eKey.wasPressedThisFrame) Debug.Log("e pressed");
        }
        #endregion
    }
}