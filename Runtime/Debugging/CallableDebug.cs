﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.General.Runtime.Debugging
{
    /// <summary>
    /// Contains public methods which can be used as debug results. Changes the color of the sprite
    /// </summary>
    [RequireComponent(typeof(SpriteRenderer))]
    public class CallableDebug : MonoBehaviour
    {
        SpriteRenderer spriteRenderer;

        private void Start()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
        }

        public void ToWhite() => spriteRenderer.color = Color.white;
        public void ToBlack() => spriteRenderer.color = Color.black;
        public void ToGreen() => spriteRenderer.color = Color.green;
        public void ToRed() => spriteRenderer.color = Color.red;
    }
}