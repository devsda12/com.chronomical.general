﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Chronomical.General.Runtime.Debugging
{
    [RequireComponent(typeof(CharacterController))]
    public class PlayerController : MonoBehaviour
    {
        #region Class variables
        //[Header("PlayerController - Required")]

        //Hidden
        CharacterController _characterController;
        #endregion

        #region Unity methods
        private void Start()
        {
            _characterController = GetComponent<CharacterController>();
        }

        private void Update()
        {
            
        }
        #endregion

        #region Input methods
        
        #endregion
    }
}