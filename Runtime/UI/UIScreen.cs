﻿using UnityEngine;
//using UnityEngine.InputSystem;

namespace Chronomical.General.Runtime.UI
{
    public class UIScreen : MonoBehaviour
    {
        #region Class variables
        [Header("Config on base UIScreen! (Childs will auto assign)")]
        public string returnKeybindName;

        [Header("AutoConfig - Don't touch!")]
        public UIScreen previousScreen;
        public UIScreen nextScreen;
        #endregion

        #region Unity methods
        private void Update()
        {
            //if (Keyboard.current.pKey.isPressed) UnityEngine.Debug.Log("hello");
        }
        #endregion
    }
}