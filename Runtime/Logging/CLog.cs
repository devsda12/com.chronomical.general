using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace Chronomical.General.Runtime.Logging
{
    #region Namespace classes
    [Serializable]
    public class LogConfigSaveWrapper
    {
        public bool enableLogging;
        public bool appendDateTime;
        public bool enableFileLogging;
        public string baseFileName;
        public bool appendDateTimeFileName;
    }
    #endregion

    /// <summary>
    /// Configurable logger which can be changed from the editor (custom window required)
    /// </summary>
    public class CLog
    {
        #region Class variables
        //Config file
        public const string configFileName = "CLog-Config.json";
        static string ConfigFilePath => Path.Combine(Application.streamingAssetsPath, configFileName);

        //General logging settings
        bool _enableLogging;
        public static bool EnableLogging
        {
            get => Instance._enableLogging;
            set => Instance._enableLogging = value;
        }

        bool _appendDateTime;
        public static bool AppendDateTime
        {
            get => Instance._appendDateTime;
            set => Instance._appendDateTime = value;
        }

        //Logging export file settings
        bool _enableFileLogging;
        public static bool EnableFileLogging
        {
            get => Instance._enableFileLogging;
            set => Instance._enableFileLogging = value;
        }

        string _baseFileName = "Chronomical-Log";
        public static string BaseFileName
        {
            get => Instance._baseFileName;
            set
            {
                if (value != "" && value.Length < 30) Instance._baseFileName = value;
                else Instance._baseFileName = "Chronomical-Log";
            }
        }

        string _filePath;
        public string FilePath
        {
            get
            {
                if(_filePath == null)
                    _filePath = Path.Combine(StaticInfo.ChronomicalPersistentDataPath, AppendDateTimeFileName ? string.Format("{0}[{1}].txt", _baseFileName, DateTime.Now.ToString("H-mm-ss")) : string.Format("{0}.txt", _baseFileName));
                return _filePath;
            }
        }

        bool _appendDateTimeFileName;
        public static bool AppendDateTimeFileName
        {
            get => Instance._appendDateTimeFileName;
            set => Instance._appendDateTimeFileName = value;
        }

        //Runtime
        static CLog _instance;
        static CLog Instance
        {
            get
            {
                if (_instance == null) _instance = new CLog();
                return _instance;
            }
            set => _instance = value;
        }
        #endregion

        #region Constructor
        CLog()
        {
            Load();
        }
        #endregion

        #region Loading / Saving
        /// <summary>
        /// Loads the config file, saves defaults if not yet exists
        /// </summary>
        void Load()
        {
            //If not yet exists, resetting to create
            if (!File.Exists(ConfigFilePath)) InternalSave();
            else
            {
                LogConfigSaveWrapper load = JsonUtility.FromJson<LogConfigSaveWrapper>(File.ReadAllText(ConfigFilePath));
                _enableLogging = load.enableLogging;
                _appendDateTime = load.appendDateTime;
                _enableFileLogging = load.enableFileLogging;
                _baseFileName = load.baseFileName;
                _appendDateTimeFileName = load.appendDateTimeFileName;
            }
        }

        /// <summary>
        /// Saves the config file
        /// </summary>
        void InternalSave()
        {
            LogConfigSaveWrapper save = new LogConfigSaveWrapper() { 
                enableLogging = _enableLogging,
                appendDateTime = _appendDateTime,
                enableFileLogging = _enableFileLogging,
                baseFileName = _baseFileName,
                appendDateTimeFileName = _appendDateTimeFileName };
            File.WriteAllText(ConfigFilePath, JsonUtility.ToJson(save, true));
        }

        /// <summary>
        /// Saves the config file for the current instance
        /// </summary>
        public static void Save() => Instance.InternalSave();
        #endregion

        #region Callable
        public static void Log(object message, object sender = null)
        {
            if (!Instance._enableLogging) return;
            string stringMessage = message.ToString();
            string add1 = sender != null ? string.Format("[{0}] - {1}", sender.ToString(), stringMessage) : stringMessage;
            string add2 = Instance._appendDateTime ? string.Format("[{0}] - {1}", DateTime.Now.ToString("H:mm:ss"), add1) : add1;

            //Debug logging it
            Debug.Log(add1);

            //File logging it
            if (Instance._enableFileLogging)
            {
                if (File.Exists(Instance.FilePath)) File.AppendAllText(Instance.FilePath, string.Format("{0}\n", add2));
                else File.WriteAllText(Instance.FilePath, string.Format("{0}\n", add2));
            }
        }

        public static void LogWarning(object message, object sender = null)
        {
            if (!Instance._enableLogging) return;
            string stringMessage = message.ToString();
            string add1 = sender != null ? string.Format("Warning - [{0}] - {1}", sender.ToString(), stringMessage) : stringMessage;
            string add2 = Instance._appendDateTime ? string.Format("[{0}] - {1}", DateTime.Now.ToString("H:mm:ss"), add1) : add1;

            //Debug logging it
            Debug.LogWarning(add1);

            //File logging it
            if (Instance._enableFileLogging)
            {
                if (File.Exists(Instance.FilePath)) File.AppendAllText(Instance.FilePath, string.Format("{0}\n", add2));
                else File.WriteAllText(Instance.FilePath, string.Format("{0}\n", add2));
            }
        }

        public static void LogError(object message, object sender = null)
        {
            if (!Instance._enableLogging) return;
            string stringMessage = message.ToString();
            string add1 = sender != null ? string.Format("Error - [{0}] - {1}", sender.ToString(), stringMessage) : stringMessage;
            string add2 = Instance._appendDateTime ? string.Format("[{0}] - {1}", DateTime.Now.ToString("H:mm:ss"), add1) : add1;

            //Debug logging it
            Debug.LogError(add1);

            //File logging it
            if (Instance._enableFileLogging)
            {
                if (File.Exists(Instance.FilePath)) File.AppendAllText(Instance.FilePath, string.Format("{0}\n", add2));
                else File.WriteAllText(Instance.FilePath, string.Format("{0}\n", add2));
            }
        }
        #endregion
    }
}