﻿using Chronomical.General.Editor.Supporting;
using Chronomical.General.Runtime.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;

namespace Chronomical.General.Editor.CustomEditors
{
    /// <summary>
    /// Custom editor for the UIScreen
    /// </summary>
    [CustomEditor(typeof(UIScreen), true)]
    public class UIScreenEditor : UnityEditor.Editor
    {
        #region Class variables
        UIScreen uiScreen;
        bool isChild;
        #endregion

        #region OnGUI
        public override void OnInspectorGUI()
        {
            if (uiScreen == null) uiScreen = target as UIScreen;

            //Running config
            ConfigureProperties();

            //Printing the properties for childs and parent
            
        }
        #endregion

        #region Object manipulation
        void ConfigureProperties()
        {
            //Checking if previous is configured and parent is a UIScreen
            UIScreen parent = uiScreen.transform.parent.GetComponent<UIScreen>();
            if(uiScreen.previousScreen == null && parent != null)
            {
                uiScreen.previousScreen = parent;
                uiScreen.returnKeybindName = parent.returnKeybindName;
                isChild = true;
            }
        }

        void ShowProperties()
        {
            //Show the keybind depending on being a parent or a child
            CommonGUI.PlaceHeader("Config on base UIScreen! (Childs will auto assign)");
            if (!isChild) uiScreen.returnKeybindName = EditorGUILayout.TextField(nameof(uiScreen.returnKeybindName), uiScreen.returnKeybindName);
            else EditorGUILayout.LabelField(nameof(uiScreen.returnKeybindName), uiScreen.returnKeybindName);

            //Show the previous screen only when a child + show the next screen
            CommonGUI.PlaceHeader("Autoconfig");
            if (isChild) EditorGUILayout.LabelField(nameof(uiScreen.previousScreen), uiScreen.previousScreen.ToString());
            EditorGUILayout.LabelField(nameof(uiScreen.nextScreen), uiScreen.nextScreen.ToString());
        }
        #endregion
    }
}