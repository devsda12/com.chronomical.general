﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Chronomical.General.Editor
{
    /// <summary>
    /// Static info used within the editor. Used for things like paths etc...
    /// </summary>
    public static class StaticEditorInfo
    {
        #region Class variables
        /// <summary>
        /// Package path relative to the root project folder
        /// </summary>
        public static string ChronomicalPackageRelPath { get => Path.Combine("Packages", "com.chronomical.general"); }

        /// <summary>
        /// Absolute package path
        /// </summary>
        public static string ChronomicalPackagePath { get => Path.Combine(Path.GetFullPath(Path.Combine(Application.dataPath, "..")), ChronomicalPackageRelPath); }

        /// <summary>
        /// Relative chronomical icon path
        /// </summary>
        public static string ChronomicalPackageIconPath { get => Path.Combine(ChronomicalPackageRelPath, "Editor", "Art", "Icon.png"); }
        #endregion
    }
}