using Chronomical.General.Editor.Supporting;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Chronomical.General.Editor.Windows
{
    /// <summary>
    /// Chronomical layer on top of a unity editor window
    /// </summary>
    public abstract class ChronomicalWindow : EditorWindow
    {
        #region Class variables
        /// <summary>
        /// Path to StreamingAssetsFolder in the editor
        /// </summary>
        protected string StreamingAssetsFolder
        {
            get
            {
                string path = Path.Combine(Application.dataPath, "StreamingAssets");
                Directory.CreateDirectory(Path.Combine(Application.dataPath, "StreamingAssets"));
                return path;
            }
        }
        #endregion

        #region GUI Methods
        public abstract void OnGUI();
        #endregion
    }
}