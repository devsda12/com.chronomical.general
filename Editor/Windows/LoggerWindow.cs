using Chronomical.General.Editor.Supporting;
using Chronomical.General.Runtime.Logging;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using static UnityEngine.GUILayout;

namespace Chronomical.General.Editor.Windows
{
    public class CLogWindow : ChronomicalWindow
    {
        #region Unity / Window methods
        [MenuItem("Chronomical/CLog")]
        public static void ShowWindow()
        {
            CLogWindow newWindow = (CLogWindow)CommonGUI.DefaultWindowSetup(GetWindow<CLogWindow>("CLog"));
        }

        private void OnDestroy()
        {
            CLog.Save();
        }
        #endregion

        #region GUI methods
        public override void OnGUI()
        {
            CommonGUI.PlaceHeader("Logger - General settings");
            if(CLog.EnableLogging = EditorGUILayout.Toggle(nameof(CLog.EnableLogging), CLog.EnableLogging))
            {
                CLog.AppendDateTime = EditorGUILayout.Toggle(nameof(CLog.AppendDateTime), CLog.AppendDateTime);

                CommonGUI.PlaceHeader("Logger - File settings");
                if (CLog.EnableFileLogging = EditorGUILayout.Toggle(nameof(CLog.EnableFileLogging), CLog.EnableFileLogging))
                {
                    CLog.BaseFileName = EditorGUILayout.TextField(nameof(CLog.BaseFileName), CLog.BaseFileName);
                    CLog.AppendDateTimeFileName = EditorGUILayout.Toggle(nameof(CLog.AppendDateTimeFileName), CLog.AppendDateTimeFileName);
                }
            }

            if (Button("Save")) CLog.Save();
        }
        #endregion
    }
}