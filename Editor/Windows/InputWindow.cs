﻿using Chronomical.General.Editor.Supporting;
using Chronomical.General.Runtime.Input;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Chronomical.General.Editor.Windows
{
    /// <summary>
    /// Editor class for the input manager. Any saving / loading of input types is done here through the editor window
    /// </summary>
    public class InputWindow : EditorWindow
    {
        #region Class variables
        //Paths
        string filePath
        {
            get
            {
                string path = Path.Combine(Application.dataPath, "StreamingAssets", InputManager.fileName);
                if (!File.Exists(path))
                {
                    Directory.CreateDirectory(Path.Combine(Application.dataPath, "StreamingAssets"));
                    File.WriteAllText(path, JsonUtility.ToJson(new SaveWrapper() { keybinds = new List<Keybind>(), axes = new List<Axis>() }, true));
                }
                return path;
            }
        }

        //Lists with keybinds and axes
        List<Keybind> keybinds = new List<Keybind>();
        List<Axis> axes = new List<Axis>();
        #endregion

        #region Unity / Window methods
        [MenuItem("Chronomical/InputManager")]
        public static void ShowWindow()
        {
            InputWindow newWindow = (InputWindow)CommonGUI.DefaultWindowSetup(GetWindow<InputWindow>("InputManager"));
            newWindow.Load();
        }
        #endregion

        #region GUI methods
        private void OnGUI()
        {
            //Keybinds
            PlaceHeader("Keybinds");
            for(int i = keybinds.Count - 1; i >= 0; i--)
            {
                ShowKeybind(keybinds[i]);
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Add Keybind...")) AddEmptyKeybind();

            //Axes
            PlaceHeader("Axes");
            for(int i = axes.Count - 1; i >= 0; i--)
            {
                ShowAxis(axes[i]);
            }
            EditorGUILayout.Space();
            if (GUILayout.Button("Add Axis...")) AddEmptyAxis();

            //Buttons
            EditorGUILayout.Space();
            PlaceHeader("File operations");
            if (GUILayout.Button("Save...")) Save();
            if (GUILayout.Button("Load...")) Load();
            if (GUILayout.Button("Clear...")) Clear();
        }

        void ShowKeybind(Keybind keybind)
        {
            //Name may not be empty, checking for this
            if (keybind.bindName.Length == 0) keybind.bindName = "[EMPTY]";

            keybind.isFolded = EditorGUILayout.Foldout(keybind.isFolded, keybind.bindName);

            //If folded out
            if (keybind.isFolded)
            {
                EditorGUI.indentLevel++;
                keybind.bindName = EditorGUILayout.TextField(nameof(keybind.bindName), keybind.bindName);
                keybind.keyCode = (KeyCode)EditorGUILayout.EnumPopup(nameof(keybind.keyCode), keybind.keyCode);

                //Delete button
                GUILayout.BeginHorizontal();
                GUILayout.Space(EditorGUI.indentLevel * 20);
                if (GUILayout.Button("Delete")) keybinds.Remove(keybind);
                GUILayout.EndHorizontal();

                EditorGUI.indentLevel--;
            }
        }

        void ShowAxis(Axis axis)
        {
            //Name may not be empty, checking for this
            if (axis.axisName == "") axis.axisName = "[EMPTY]";

            axis.isFolded = EditorGUILayout.Foldout(axis.isFolded, axis.axisName);

            //If folded out
            if (axis.isFolded)
            {
                EditorGUI.indentLevel++;
                axis.axisName = EditorGUILayout.TextField(nameof(axis.axisName), axis.axisName);
                axis.positive = (KeyCode)EditorGUILayout.EnumPopup(nameof(axis.positive), axis.positive);
                axis.negative = (KeyCode)EditorGUILayout.EnumPopup(nameof(axis.negative), axis.negative);
                axis.valueGravity = Mathf.Clamp(EditorGUILayout.FloatField(nameof(axis.valueGravity), axis.valueGravity), 0.001f, 100);

                //Delete button
                GUILayout.BeginHorizontal();
                GUILayout.Space(EditorGUI.indentLevel * 20);
                if (GUILayout.Button("Delete")) axes.Remove(axis);
                GUILayout.EndHorizontal();

                EditorGUI.indentLevel--;
            }
        }
        #endregion

        #region Keybind / Axis changing methods
        void AddEmptyKeybind()
        {
            keybinds.Add(new Keybind() { bindName = "[EMPTY]" });
        }

        void AddEmptyAxis()
        {
            axes.Add(new Axis() { axisName = "[EMPTY]" });
        }
        #endregion

        #region Misc. GUI methods
        void PlaceHeader(string headerTitle)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(headerTitle, EditorStyles.boldLabel);
        }
        #endregion

        #region File operations
        /// <summary>
        /// Saves data to the StreamingAssets folder
        /// </summary>
        void Save()
        {
            SaveWrapper save = new SaveWrapper() { keybinds=keybinds, axes=axes };
            File.WriteAllText(filePath, JsonUtility.ToJson(save, true));
            Debug.Log("Save successful");
        }

        /// <summary>
        /// Loads data from the StreamingAssets folder
        /// </summary>
        void Load()
        {
            SaveWrapper load = JsonUtility.FromJson<SaveWrapper>(File.ReadAllText(filePath));
            keybinds = load.keybinds;
            axes = load.axes;
            Debug.Log("Load successful");
        }

        /// <summary>
        /// Clears all keybinds from memory
        /// </summary>
        void Clear() 
        { 
            keybinds.Clear(); 
            axes.Clear();
            Debug.Log("Clear successful");
        }
        #endregion
    }
}
