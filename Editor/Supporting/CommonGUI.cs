﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Chronomical.General.Editor.Supporting
{
    /// <summary>
    /// Contains common GUI methods which can be used create certain elements within the Unity editor GUI
    /// </summary>
    public static class CommonGUI
    {
        #region GUI methods
        /// <summary>
        /// Places a header similar to Unity's Header attribute
        /// </summary>
        /// <param name="headerTitle">The title of the header to place</param>
        public static void PlaceHeader(string headerTitle)
        {
            EditorGUILayout.Space();
            EditorGUILayout.LabelField(headerTitle, EditorStyles.boldLabel);
        }
        #endregion

        #region Window specific methods
        /// <summary>
        /// Sets up some default properties related to any created Chronomical windows
        /// </summary>
        /// <param name="editorWindow">The newly created editor window</param>
        /// <returns>The sent editor window which was just modified</returns>
        public static EditorWindow DefaultWindowSetup(EditorWindow editorWindow)
        {
            //Setting the icon
            editorWindow.titleContent.image = AssetDatabase.LoadAssetAtPath<Texture>(StaticEditorInfo.ChronomicalPackageIconPath) as Texture;

            return editorWindow;
        }
        #endregion
    }
}
